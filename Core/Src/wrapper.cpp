#include "wrapper.hpp"

/* Include Default Begin */

/* Include Default End */


/* Include Private Begin */
#include "i2c.h"
#include "tim.h"
#include "gpio.h"
#include <stdint.h>
/* Include Private End */

/* Class Constructor Begin */
#define ADDRESS 0x6A<<1//角速度、加速度のレジスタのI2Cのアドレス
#define GYRO_REGISTA_START 0x18//角速度のデータが入っているレジスタのアドレス
#define FRENQUENCY 1000.0//tim6の周波数
/* Class Constructor End */
#define DEBUG_MODE

/* Variable Begin */
int16_t angularVelocity[3] = {0,0,0};
int16_t angularVelocityError[3] = {0,0,0};
float deg[3];
#ifdef DEBUG_MODE
uint8_t progressState = 0;
uint16_t Fcount[3] = {0,0,0};
#endif
/* Variable End */

void init(void){
	HAL_TIM_Base_Start_IT(&htim6);
	uint8_t gyroConfig[]={0b00101010,0b00000000,0b00001001};//CTRL_REG1_G,CTRL_REG2_G,CTRL_REG3_G
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x10,1,gyroConfig,sizeof(gyroConfig),100)){
		progressState = 1;
	}
	uint8_t on = 0b00111000;
	//CTRL_REG4
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x1E,1,&on,1,100)){
		progressState = 2;
	}

	uint8_t check = 0b00000100;
	while(HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x24,1,&check,1,100)){
		progressState = 3;
	}
	progressState = 4;



}

void loop(void){

}

/* Function Body Begin */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim == &htim6){
		setConfig();
		getValue();
		for(uint8_t i=0;i<3;i++){
			setCriteria(i);
			useThresholdFilta(i,10);
		}
		calcValue();
	}

}
void getValue(){
	uint8_t data[6];
	HAL_I2C_Mem_Read(&hi2c1,ADDRESS,GYRO_REGISTA_START,1,(uint8_t*)data,sizeof(data),0xFF);
	for(uint8_t i = 0;i<3;i++){
		angularVelocity[i] = (int16_t)data[2*i+1]<<8 | data[2*i];
		angularVelocity[i] -= angularVelocityError[i];
	}
}
void setConfig(){
	uint8_t gyroConfig[]={0b00101010,0b00000000,0b00001001};//CTRL_REG1_G,CTRL_REG2_G,CTRL_REG3_G
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x10,1,gyroConfig,sizeof(gyroConfig),100);
	uint8_t on = 0b00111000;
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x1E,1,&on,1,100);
	uint8_t check = 0b00000100;
	HAL_I2C_Mem_Write(&hi2c1,ADDRESS,0x24,1,&check,1,100);
}
void setCriteria(uint8_t number){
	static int16_t beforeAngularVelocity[3] = {0,0,0};
	static uint32_t tickValue;
	static uint8_t fixCount[3] = {0,0,0};
	if(!(beforeAngularVelocity[number]-10 < angularVelocity[number] && angularVelocity[number] < beforeAngularVelocity[number]+10)){
		tickValue = HAL_GetTick();
		beforeAngularVelocity[number] = angularVelocity[number];
	}
	if(HAL_GetTick() - tickValue > 40 && fixCount[number] < 2){
		if(fixCount[number] == 0){
			angularVelocityError[number] = angularVelocity[number];
		}
		fixCount[number]++;
		deg[number] = 0;
	}
	Fcount[number] = fixCount[number];
}
void calcValue(){
	static float beforeFixedAngularVelocity[3] = {0,0,0};
	static float fixedAngularVelocity[3] = {0,0,0};
	for(uint8_t i=0;i<3;i++){
		beforeFixedAngularVelocity[i] = fixedAngularVelocity[i];
		fixedAngularVelocity[i] = angularVelocity[i]/27.0;//
		deg[i] += (fixedAngularVelocity[i]+beforeFixedAngularVelocity[i])/FRENQUENCY/2;
	}

}
void useThresholdFilta(uint8_t number,float thresholdValue){
	if(-thresholdValue < angularVelocity[number] && angularVelocity[number] < thresholdValue){
		angularVelocity[number] = 0;
	}
}

/* Function Body End */
